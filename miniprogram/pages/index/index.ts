// index.ts
// 获取应用实例
import NoticeApi from "../../api/NoticeApi"
import Toast from "tdesign-miniprogram/toast/index"

Page({
  data: {
    list: []
  },
  onLoad(): void | Promise<void> {
    this.getNotice()
  },
  jump(e: any) {
    const target: string = e.currentTarget.dataset.target
    wx.navigateTo({
      url: target
    })
  },
  handleMore() {
    wx.navigateTo({
      url: '/pages/notice/list/index'
    })
  },
  getNotice() {
    NoticeApi.page({ index: 1, size: 3 }).then((res: any) => {
      this.setData({ list: res.data })
    }).catch(e => {
      Toast({
        context: this,
        selector: '#t-toast',
        message: e,
      })
    })
  },
  handleNotice(e: any) {
    const id: string = e.currentTarget.dataset.id
    wx.navigateTo({
      url: `/pages/notice/detail/index?id=${id}`
    })
  },
})
