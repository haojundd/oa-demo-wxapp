// pages/attendance/deal/index.ts
import Toast from "tdesign-miniprogram/toast"
import AttendanceApi from "../../../api/AttendanceApi"

enum AttendStatusEnum {
  NORMAL = 'NORMAL',
  PENDING = 'PENDING',
  PROCESSING = 'PROCESSING',
  REJECT = 'REJECT',
  PASSED = 'PASSED',
}

Page({

  /**
   * 页面的初始数据
   */
  data: {
    mode: '',
    typeLabel: '请选择',
    id: 0,
    type: null,
    remark: '',
    signDate: '',
    signTime: '',
    exitTime: '',
    typeList: [
      { label: '迟到', value: 'LATE_ATTEND' },
      { label: '早退', value: 'EARLY_ABSENCE' },
      { label: '漏卡', value: 'FORGOT' },
      { label: '外勤', value: 'OUTSIDE' },
      { label: '其他', value: 'OTHER' },
    ],
    fileList: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options: any) {
    let id: number = Number(options.id)
    let signDate = options.signDate
    this.setData({ signDate })
    if (!id) return
    AttendanceApi.getById(id).then((res: any) => {
      let row = res.data[0]
      this.setData({
        ...row,
        signTimeVal: row.signTime,
        exitTimeVal: row.exitTime,
        typeValue: [row.type]
      })
    }).catch(e => {
      Toast({
        context: this,
        selector: '#t-toast',
        message: e,
      })
    })
  },
  onTypePicker() {
    this.setData({ typeVisible: true })
  },
  onPickerChange(e: any) {
    const { key } = e.currentTarget.dataset
    const { label, value } = e.detail
    this.setData({
      [`${key}Visible`]: false,
      [`${key}Value`]: value,
      [key]: value[0],
      [`${key}Label`]: label.join(' '),
    })
  },
  onPickerCancel(e: any) {
    const { key } = e.currentTarget.dataset
    this.setData({
      [`${key}Visible`]: false,
    })
  },
  // showPicker(e: any) {
  //   if (this.data.type !== 'FORGOT') return
  //   const { mode } = e.currentTarget.dataset
  //   this.setData({
  //     mode,
  //     [`${mode}TimeVisible`]: true,
  //   })
  // },
  hidePicker() {
    const { mode } = this.data
    this.setData({
      [`${mode}TimeVisible`]: false,
    })
  },
  onConfirm(e: any) {
    const { value } = e.detail
    const { mode } = this.data

    this.setData({
      [`${mode}Time`]: value,
    })

    this.hidePicker()
  },
  handleAdd(e: any) {
    const { files } = e.detail
    // @ts-ignore
    this.setData({ fileList: [...files] })
  },
  handleRemove(e: any) {
    const { index } = e.detail
    const { fileList } = this.data

    fileList.splice(index, 1)
    this.setData({
      fileList,
    })
  },
  async uploadFile(file: any): Promise<string> {
    let res: any = await AttendanceApi.uploadAnnex(file.url)
    return res?.data[0]
  },
  async handleConfirm() {
    let { id, type, remark, fileList, signTime, exitTime, signDate } = this.data
    if (!type) {
      Toast({
        context: this,
        selector: '#t-toast',
        message: '请选择类型',
      })
      return
    }
    if (!remark) {
      Toast({
        context: this,
        selector: '#t-toast',
        message: '请填写说明',
      })
      return
    }
    let snapshot: string = ''
    if (fileList.length) {
      snapshot = await this.uploadFile(fileList[0])
    }
    AttendanceApi.handle({
      id,
      type,
      status: AttendStatusEnum.PROCESSING,
      signDate,
      signTime,
      exitTime,
      remark,
      snapshot
    }).then(_ => {
      Toast({
        context: this,
        selector: '#t-toast',
        message: '处理成功',
      })
      setTimeout(() => {
        wx.navigateBack()
      }, 300)
    }).catch(e => {
      Toast({
        context: this,
        selector: '#t-toast',
        message: e,
      })
    })
  },
})
