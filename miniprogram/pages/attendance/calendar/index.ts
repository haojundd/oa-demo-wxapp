import AttendanceApi from "../../../api/AttendanceApi"
import Toast from "tdesign-miniprogram/toast/index"
import { DateUtil, formatToDate } from "../../../utils/DateUtil"

enum AttendStatusEnum {
  NORMAL = 'NORMAL',
  PENDING = 'PENDING',
  PROCESSING = 'PROCESSING',
  REJECT = 'REJECT',
  PASSED = 'PASSED',
}

Page({
  /**
   * 页面的初始数据
   */
  data: {
    id: 0,
    minDate: DateUtil().add(-1, 'month').startOf('month').toDate().getTime(),
    maxDate: DateUtil().add(-1, 'day').toDate().getTime(),
    currDate: DateUtil().add(-1, 'day').toDate().getTime(),
  },
  signMap: new Map(),
  /**
   * 生命周期函数--监听页面加载
   */
  onShow() {
    this.queryDate()
  },
  handleSelect(e: { detail: { value: number } }) {
    const { value } = e.detail
    this.setData({ currDate: value })
    let date = formatToDate(value)
    let element = this.signMap.get(date)
    if (element) {
      let { signTime, exitTime, status, id, typeLabel, remark } = element
      this.setData({
        id,
        signTime: signTime || '--:--:--',
        exitTime: exitTime || '--:--:--',
        signStatus: status,
        typeLabel,
        remark
      })
    } else {
      this.setData({
        signTime: '--:--:--',
        exitTime: '--:--:--',
        signStatus: '',
        id: 0,
        typeLabel: '',
        remark: ''
      })
    }
  },
  queryDate() {
    let { userId } = wx.getStorageSync("user")
    AttendanceApi.list({
      userId,
      startDate: DateUtil().add(-1, 'month').startOf('month').format('YYYY-MM-DD')
    }).then((res: any) => {
      let signMap = new Map()
      res.data.forEach((i: Attendance) => {
        signMap.set(i.signDate, i)
      })
      this.signMap = signMap

      function format(day: any) {
        const { date } = day
        if (date.getTime() > DateUtil().add(-1, 'day').toDate().getTime()) {
          return day
        }
        let formatDate = formatToDate(date)
        const isWeekend = [0, 6].includes(date.getDay())
        let element = signMap.get(formatDate)
        let status = element?.status || AttendStatusEnum.PENDING
        if (!isWeekend) {
          switch (status) {
            case AttendStatusEnum.PENDING:
            case AttendStatusEnum.REJECT:
              day.prefix = '•'
              day.className = 'has-question'
              break
            case AttendStatusEnum.PROCESSING:
              day.className = 'has-question'
              break
            case AttendStatusEnum.NORMAL:
              day.suffix = '正常'
              break
            case AttendStatusEnum.PASSED:
              day.suffix = '通过'
              break
          }
        }
        return day
      }

      this.setData({ format })
      this.handleSelect({ detail: { value: this.data.currDate } })
    }).catch(e => {
      Toast({
        context: this,
        selector: '#t-toast',
        message: e,
      })
    })
  },
  handleDeal() {
    wx.navigateTo({
      url: `/pages/attendance/deal/index?id=${this.data.id}&signDate=${formatToDate(this.data.currDate)}`
    })
  },

})
