/**
 * 日期时间工具
 */
import dayjs from 'dayjs';

const DATE_TIME_FORMAT = 'YYYY-MM-DD HH:mm:ss';
const DATE_FORMAT = 'YYYY-MM-DD';

export function formatToDateTime(date?: dayjs.ConfigType, format = DATE_TIME_FORMAT): string {
  return dayjs(date).format(format);
}

export function formatToDate(date?: dayjs.ConfigType, format = DATE_FORMAT): string {
  return dayjs(date).format(format);
}

export function hourInterval(date1?: dayjs.ConfigType, date2?: dayjs.ConfigType): number {
  return dayjs(date2).diff(dayjs(date1), 'hour');
}

export function minuteInterval(date1?: dayjs.ConfigType, date2?: dayjs.ConfigType): number {
  return dayjs(date2).diff(dayjs(date1), 'minute');
}

export function timeIntervalWord(date1?: dayjs.ConfigType, date2?: dayjs.ConfigType): string {
  const hours = hourInterval(date1, date2);
  const minutes = minuteInterval(date1, date2);
  const floats = minutes - hours * 60;
  return `${hours ? hours + ' h' : ''} ${floats} m`;
}

export const DateUtil = dayjs;
