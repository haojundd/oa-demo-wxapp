# oa-demo-wxapp

## 简介

本项目为 OA Demo 系统的微信小程序部分，基于 TDesign+ts+less 开发

## 配套项目

后端项目 https://gitee.com/haojundd/oa-demo-server.git

前端项目 https://gitee.com/haojundd/oa-demo-admin.git

微信小程序 https://gitee.com/haojundd/oa-demo-wxapp.git

## 快速开始

1、安装依赖

```shell
npm install
```

2、点击开发者工具中的菜单栏：工具 --> 构建 npm

<img style="height: 360px" src="doc-imgs/img.png">

## 项目结构

```text
.
├── api                     #接口
├── imgs                    #图片
├── pages                   #页面
│   ├── absence
│   ├── attendance
│   ├── employee
│   ├── expense
│   ├── index
│   ├── login
│   ├── notice
│   └── workcard
├── utils                   #工具
├── app.json
├── app.less
├── app.ts
├── package-lock.json
├── package.json
└── sitemap.json
```

## 界面展示

<table>
<tr>
<td><img src="doc-imgs/login.png"/></td>
<td><img src="doc-imgs/office.png"/></td>
</tr>
<tr>
<td><img src="doc-imgs/sign.png"/></td>
<td><img src="doc-imgs/calendar.png"/></td>
</tr>
<tr>
<td><img src="doc-imgs/handle.png"/></td>
<td><img src="doc-imgs/card.png"/></td>
</tr>
<tr>
<td><img src="doc-imgs/absence.png"/></td>
<td><img src="doc-imgs/absence1.png"/></td>
</tr>
<tr>
<td><img src="doc-imgs/expense.png"/></td>
<td><img src="doc-imgs/notice.png"/></td>
</tr>
</table>

